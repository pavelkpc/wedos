<?php
$login = 'pkloud@kpcomp.cz';
$password = 'a62+jA36j:b-MS';
$test = false;
$debug = true;
$importFile = 'domainslist.dat';

################################

$wedos = new wedos($login, $password);
$wedos->setTest($test);
$wedos->setDebug($debug);

// Nacteni domen ze souboru
$fileDomains = array();
$webDomains = array();
$lines = file($importFile);
foreach ($lines as $line_num => $line) {
	$fileDomains[] = trim($line);
}

// Prochazime domeny na webu, ty ktere nejsou v souboru, se odstrani
if($res = $wedos->domainsList()->getData()) {
	foreach ($res->domain as $domain) {
		if (!in_array($domain->name, $fileDomains)) {
			$res1 = $wedos->domainDelete($domain->name);
		} else {
			$webDomains[] = $domain->name;
		}
	}
}

// Zaradi nove domeny v souboru na web
foreach ($fileDomains as $domain) {
	if (!in_array($domain, $webDomains)) {
		$res2 = $wedos->domainAdd($domain, wedos::DOMAIN_SECONDARY, 0, array(), '85.118.129.245');
	}
}

// Spusti axfr na vsechny domeny s souboru
foreach ($fileDomains as $domain) {
	$res3 = $wedos->axfrRun($domain);
}

echo "Vysledek scriptu Wedos ".date('Y-m-d H:i:s')."<br />\n";
echo "-------------------------------------------------------<br />\n";
echo "Domen v souboru: ".count($fileDomains) . "<br />\n";
echo "Domen na webu: ".count($webDomains) . "<br />\n";
echo implode("<br />\n", $wedos->getReport());
if ($debug) {
	echo "<br />\n";
	echo "<pre>";
	print_r($wedos->getDebugData());
	echo "</pre>";
}
exit;


/**
 * Created by PhpStorm.
 * User: Diwi
 * Date: 07.07.2016
 * Time: 17:12
 */
class wedos
{
	const DOMAIN_PRIMARY = 'primární';
	const DOMAIN_SECONDARY = 'sekundární';

	/**
	 * Testovaci request na wedos
	 *
	 * @var bool
	 */
	private $test = false;

	/**
	 * Debug rezim zaznamenava requesty a response
	 *
	 * @var bool
	 */
	private $debug = false;
	private $debugData = array();
	private $url = 'https://api.wedos.com/wapi/xml';
	private $password;
	private $login;
	private $lastRequest;
	private $lastResponse;
	private $report = array();

	/**
	 * @var request
	 */
	private $request;

	/**
	 * @var response
	 */
	private $response;

	public function __construct($login, $password)
	{
		$this->login = $login;
		$this->password = $password;
	}

	/**
	 * @return response
	 */
	public function ping()
	{
		$xml = $this->getRequest()
			->command('ping');

		return $this->callWedos($xml);
	}

	/**
	 * Seznam domen
	 *
	 * @return response
	 */
	public function domainsList()
	{
		$xml = $this->getRequest()
			->command('dns-domains-list');

		$res = $this->callWedos($xml);
		$this->setReport('dns-domains-list', '-', $res);

		return $res;
	}

	/**
	 * Vymazani domeny
	 *
	 * @param $name
	 *
	 * @return response
	 */
	public function domainDelete($name)
	{
		$xml = $this->getRequest()
			->command('dns-domain-delete')
			->data(array(
				'name' => $name
			));

		$res = $this->callWedos($xml);
		$this->setReport('dns-domain-delete', $name, $res);

		return $res;
	}

	/**
	 * Vlozeni domeny
	 *
	 * @param string $name název domény - povinný parametr
	 * @param string $type typ záznamu domény (primární/sekundární) - nepovinné (výchozí: primární)
	 * @param int    $axfr_enabled povoleno odchozí axfr? (0/1) - pouze u primární domény - nepovinné (výchozí: 0)
	 * @param array  $axfr_ip seznam povolených ip adres pro axfr (pouze u primárního záznamu, vyžadováno, pokud axfr_enabled=1)
	 * @param string $primary_ip ip adresa dns serveru s primárním záznamem domény - vyžadováno u sekundárního záznamu
	 *
	 * @return response
	 */
	public function domainAdd($name, $type = 'primární', $axfr_enabled = 0, $axfr_ip = array(), $primary_ip = '')
	{
		$data = array(
			'name' => $name,
			'type' => $type,
			'axfr_enabled' => $axfr_enabled,
		);
		if ($axfr_ip) {
			$data['axfr_ip'] = array('ip' => $axfr_ip);
		}
		if ($primary_ip) {
			$data['primary_ip'] = $primary_ip;
		}

		$xml = $this->getRequest()
			->command('dns-domain-add')
			->data($data);

		$res = $this->callWedos($xml);
		$this->setReport('dns-domain-add', $name, $res);

		return $res;
	}

	/**
	 * Spusteni axfr na domene
	 *
	 * @param string $domain
	 *
	 * @return response
	 */
	public function axfrRun($domain)
	{
		$xml = $this->getRequest()
			->command('dns-domain-axfr-run')
			->data(array(
				'name' => $domain
			));

		$res = $this->callWedos($xml);
		$this->setReport('dns-domain-axfr-run', $domain, $res);

		return $res;
	}

	/**
	 * @return requestXML
	 */
	private function getRequest()
	{
		$this->request = new request($this->login, $this->password, $this->isTest());
		return $this->request->getXML();
	}

	/**
	 * @return bool|string
	 */
	public function getLastRequest()
	{
		return $this->lastRequest;
	}

	/**
	 * @param $responseXML
	 *
	 * @return response
	 */
	private function getResponse($responseXML)
	{
		$this->response = new response($responseXML);
		return $this->response;
	}

	/**
	 * @return SimpleXMLElement
	 */
	public function getLastResponse()
	{
		return $this->lastResponse;
	}

	/**
	 * @param requestXML $xml
	 *
	 * @return response
	 */
	private function callWedos($xml)
	{
		$this->lastRequest = $xml->close();
		// POST data
		$post = 'request=' . urlencode($this->lastRequest);

		// inicializace cURL session
		$ch = curl_init();

		// nastavení URL a POST dat
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// odpověď chceme jako návratovou hodnotu curl_exec()
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// doba, po kterou skript čeká na odpověď
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);

		// vypnutí kontrol SSL certifikátů
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		// provedení volání
		$res = curl_exec($ch);

		$this->lastResponse = $this->getResponse($res);

		$this->setDebugData($this->lastRequest, $this->lastResponse);

		return $this->lastResponse;
	}

	/**
	 * @return boolean
	 */
	public function isTest()
	{
		return $this->test;
	}

	/**
	 * @param boolean $test
	 */
	public function setTest($test)
	{
		$this->test = $test;
	}

	/**
	 * @return boolean
	 */
	public function isDebug()
	{
		return $this->debug;
	}

	/**
	 * Zaznamenava vsechny requesty a response
	 *
	 * @param boolean $debug
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}

	public function getDebugData()
	{
		return $this->debugData;
	}

	/**
	 * Nastavuje debug data
	 *
	 * @param string $requestXML
	 * @param response $response
	 *
	 * @return bool
	 */
	private function setDebugData($requestXML, $response)
	{
		if (!$this->isDebug()) {
			return false;
		}

		$this->debugData[] = array(
			'request' => $requestXML,
			'response' => print_r($response, true),
		);

		return true;
	}

	/**
	 * Sestaveni reportu z vysledku volani
	 *
	 * @param string $operation
	 * @param string $domain
	 * @param response $response
	 */
	private function setReport($operation, $domain, $response)
	{
		$result = $response->getResult() == 'OK' ? 'OK' : 'ERROR (' . $response->getCode() . ') ' . $response->getResult();
		$this->report[] = "Operace '<b>{$operation}</b>' na domene '<b>{$domain}</b>': " . $result;
	}

	/**
	 * Vraceni seznamu vsech zaznamenanych reportu
	 *
	 * @return array
	 */
	public function getReport()
	{
		return $this->report;
	}

}

class response
{
	private $object;

	public function __construct($xmlData)
	{
		$this->object = simplexml_load_string($xmlData);
	}

	private function getObject()
	{
		return $this->object;
	}

	public function getData()
	{
		return $this->object && property_exists($this->object, 'data') ? $this->object->data : false;
	}

	public function getCode()
	{
		return $this->object && property_exists($this->object, 'code') ? $this->object->code : false;
	}

	public function getResult()
	{
		return $this->object && property_exists($this->object, 'result') ? $this->object->result : false;
	}
}

class request
{
	/**
	 * @var requestXML
	 */
	private $xml;

	/**
	 * Generuje zakladni request XML
	 *
	 * @param            $login
	 * @param            $password
	 * @param bool|false $test
	 */
	public function __construct($login, $password, $test = false)
	{
		$requestId = $this->generateId();
		$auth = $this->getAuth($login, $password);

		$this->xml = new requestXML();
		$this->xml
			->auth($login, $auth)
			->id($requestId);

		if ($test) {
			$this->xml->test();
		}

		return $this->xml;
	}

	/**
	 * Vygeneruje hash auth
	 * @param $login
	 * @param $password
	 *
	 * @return $this
	 */
	private function getAuth($login, $password)
	{
		$auth = sha1($login . sha1($password) . date('H', time()));

		return $auth;
	}

	private function generateId()
	{
		return md5(microtime(true));
	}

	/**
	 * @return requestXML
	 */
	public function getXML()
	{
		return $this->xml;
	}
}

class requestXML
{
	protected $xml;

	public function __construct()
	{
		$this->xml = new XMLWriter();
		$this->xml->openMemory();
		$this->xml->startDocument('1.0', 'utf-8');
		$this->xml->setIndent(true);
		$this->xml->setIndentString('    ');

		$this->xml->startElement('request');
	}

	/**
	 * Authorizace
	 * @param $login
	 * @param $auth
	 *
	 * @return $this
	 */
	public function auth($login, $auth)
	{
		$this->xml->startElement('user');
		$this->xml->text($login);
		$this->xml->endElement();
		$this->xml->startElement('auth');
		$this->xml->text($auth);
		$this->xml->endElement();

		return $this;
	}

	/**
	 * Identifikator requestu
	 * @param $id
	 *
	 * @return $this
	 */
	public function id($id)
	{
		$this->xml->startElement('clTRID');
		$this->xml->text($id);
		$this->xml->endElement();

		return $this;
	}

	/**
	 * Prikaz
	 * @param $name
	 *
	 * @return $this
	 */
	public function command($name)
	{
		$this->xml->startElement('command');
		$this->xml->text($name);
		$this->xml->endElement();

		return $this;
	}

	/**
	 * Vklada doplnujici parametry
	 * @param $params
	 *
	 * @return $this
	 */
	public function data($params)
	{
		$this->xml->startElement('data');

		foreach ($params as $name => $value) {
			$this->xml->startElement($name);

			if (is_array($value)) {
				foreach ($value as $subName => $subValue) {
					if (is_array($subValue)) {
						foreach ($subValue as $subsubValue) {
							$this->xml->startElement($subName);
							$this->xml->text($subsubValue);
							$this->xml->endElement();
						}
					} else {
						$this->xml->text($value);
					}
				}
			} else {
				$this->xml->text($value);
			}

			$this->xml->endElement();
		}

		$this->xml->endElement();

		return $this;
	}

	/**
	 * Zapne testovaci mod
	 *
	 * @return $this
	 */
	public function test()
	{
		$this->xml->startElement('test');
		$this->xml->text(1);
		$this->xml->endElement();

		return $this;
	}

	/**
	 * Uzavreni XML souboru
	 *
	 * @return bool|string - pokud byl vystup do pameti, vraci XML dokument
	 */
	public function close()
	{
		$this->xml->endDocument(); // CLOSE ALL OPENED ELEMENTS

		$buffer = $this->xml->outputMemory();
		return $buffer;
	}

}
